package com.endrjudev.tomtomtask.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class Doc implements Parcelable {

    public static final DiffUtil.ItemCallback<Doc> callback = new DiffUtil.ItemCallback<Doc>() {
        @Override
        public boolean areItemsTheSame(@NonNull Doc oldItem, @NonNull Doc newItem) {
            return oldItem.title.equals(newItem.title);
        }

        @Override
        public boolean areContentsTheSame(@NonNull Doc oldItem, @NonNull Doc newItem) {
            return oldItem.equals(newItem);
        }
    };

    public static final Creator<Doc> CREATOR = new Creator<Doc>() {
        @Override
        public Doc createFromParcel(Parcel in) {
            return new Doc(in);
        }

        @Override
        public Doc[] newArray(int size) {
            return new Doc[size];
        }
    };

    @SerializedName("author_name")
    private final List<String> author;
    @SerializedName("title")
    private final String title;
    @SerializedName("isbn")
    private final List<String> isbnList;

    public Doc(List<String> author,
               String title,
               List<String> isbnList) {
        this.author = author;
        this.title = title;
        this.isbnList = isbnList;
    }

    protected Doc(Parcel in) {
        author = in.createStringArrayList();
        title = in.readString();
        isbnList = in.createStringArrayList();
    }

    @NonNull
    public List<String> getAuthor() {
        return author;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public List<String> getIsbnList() {
        return isbnList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Doc doc = (Doc) o;
        return Objects.equals(author, doc.author) &&
                Objects.equals(isbnList, doc.isbnList) &&
                Objects.equals(title, doc.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, isbnList, title);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(author);
        dest.writeString(title);
        dest.writeStringList(isbnList);
    }
}