package com.endrjudev.tomtomtask.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DocsResponse {

    @SerializedName("docs")
    private final List<Doc> docList;

    public DocsResponse(List<Doc> docList) {
        this.docList = docList;
    }

    public List<Doc> getDocList() {
        return docList;
    }

}
