package com.endrjudev.tomtomtask.network.callback;

import com.endrjudev.tomtomtask.network.Response;

public abstract class SimpleNetworkCallback<T> implements NetworkCallback<T> {
    @Override
    public void onSuccess(Response<T> response) {

    }

    @Override
    public void onFailure(Response<T> response) {

    }

    @Override
    public void onStart(Response<T> response) {

    }

    @Override
    public void onCancel() {

    }
}
