package com.endrjudev.tomtomtask.network.adapter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

public class BaseAdapter<T> implements ResponseAdapter<T> {

    private final Class<T> clazz;
    private final Gson gson;

    public BaseAdapter(Class<T> clazz) {
        this.clazz = clazz;
        this.gson = new GsonBuilder().create();
    }

    @Override
    public T parse(InputStream stream) {
        final Reader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
        final Type type = TypeToken.get(clazz).getType();
        return gson.fromJson(reader, type);
    }
}
