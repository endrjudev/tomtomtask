package com.endrjudev.tomtomtask.network.callback;


import com.endrjudev.tomtomtask.network.Response;

public interface NetworkCallback<T> {
    void onSuccess(Response<T> response);

    void onFailure(Response<T> response);

    void onStart(Response<T> response);

    void onCancel();
}

