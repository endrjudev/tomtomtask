package com.endrjudev.tomtomtask.network.adapter;

import java.io.InputStream;

public interface ResponseAdapter<T> {
    T parse(InputStream stream);
}
