package com.endrjudev.tomtomtask.network;

import android.os.AsyncTask;

import com.endrjudev.tomtomtask.network.adapter.ResponseAdapter;
import com.endrjudev.tomtomtask.network.callback.NetworkCallback;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class NetworkCall<T> {

    private final CallType callType;
    private final ResponseAdapter<T> adapter;

    private NetworkCallTask task;

    public NetworkCall(CallType callType, ResponseAdapter<T> adapter) {
        this.callType = callType;
        this.adapter = adapter;
    }

    public void executeCall(String requestUrl, NetworkCallback<T> callback) {
        task = new NetworkCallTask(requestUrl, callback);
        task.execute();
    }

    public void cancelTask() {
        if (task != null) {
            task.cancel(true);
        }
    }

    private class NetworkCallTask extends AsyncTask<Void, Void, T> {

        private final NetworkCallback<T> callback;
        private final String requestUrl;

        private NetworkCallTask(String requestUrl, NetworkCallback<T> callback) {
            this.callback = callback;
            this.requestUrl = requestUrl;
        }

        @Override
        protected T doInBackground(Void... params) {
            T response = null;
            HttpsURLConnection connection = null;

            try {
                final URL fullRequestUrl = new URL(requestUrl);

                connection = ((HttpsURLConnection) fullRequestUrl.openConnection());
                connection.setRequestMethod(callType.typeName);
                connection.addRequestProperty("Accept", "application/json");

                final InputStream inputStream;

                if (hasError(connection)) {
                    inputStream = connection.getErrorStream();
                    throw new IOException(inputStream.toString());
                } else {
                    inputStream = connection.getInputStream();
                    response = adapter.parse(inputStream);
                }

            } catch (IOException exception) {
                callback.onFailure(new Response<>(ResponseStatus.ERROR, exception));
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }

            return response;
        }

        @Override
        protected void onPostExecute(T t) {
            if (t != null) {
                callback.onSuccess(new Response<>(t, ResponseStatus.SUCCESS));
            } else {
                callback.onFailure(new Response<>(null, ResponseStatus.ERROR));
            }
        }

        @Override
        protected void onPreExecute() {
            callback.onStart(new Response<>(null, ResponseStatus.PROGRESS));
        }

        @Override
        protected void onCancelled() {
            callback.onCancel();
        }

        private boolean hasError(HttpURLConnection connection) throws IOException {
            return connection.getResponseCode() / 100 != 2;
        }

    }
}