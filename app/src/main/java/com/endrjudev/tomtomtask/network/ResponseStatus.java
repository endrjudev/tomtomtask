package com.endrjudev.tomtomtask.network;

public enum ResponseStatus {
    SUCCESS,
    ERROR,
    PROGRESS
}