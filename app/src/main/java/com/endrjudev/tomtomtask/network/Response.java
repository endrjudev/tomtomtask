package com.endrjudev.tomtomtask.network;

public class Response<T> {
    private final T data;
    private final ResponseStatus status;
    private final Throwable exception;

    public Response(T data, ResponseStatus status) {
        this.data = data;
        this.status = status;
        this.exception = null;
    }

    public Response(ResponseStatus status, Throwable exception) {
        this.status = status;
        this.exception = exception;
        this.data = null;
    }

    public T getData() {
        return data;
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public Throwable getException() {
        return exception;
    }


}