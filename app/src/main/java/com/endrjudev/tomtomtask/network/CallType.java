package com.endrjudev.tomtomtask.network;

public enum CallType {
    GET("GET"),
    POST("POST"),
    PUT("PUT"),
    DELETE("DELETE");

    final String typeName;

    CallType(String typeName) {
        this.typeName = typeName;
    }
}
