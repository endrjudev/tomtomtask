package com.endrjudev.tomtomtask.network.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.InputStream;

public class BitmapAdapter implements ResponseAdapter<Bitmap> {

    @Override
    public Bitmap parse(InputStream stream) {
        return BitmapFactory.decodeStream(stream);
    }
}
