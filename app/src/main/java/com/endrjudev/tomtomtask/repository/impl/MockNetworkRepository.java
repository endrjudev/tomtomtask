package com.endrjudev.tomtomtask.repository.impl;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.endrjudev.tomtomtask.model.Doc;
import com.endrjudev.tomtomtask.model.DocsResponse;
import com.endrjudev.tomtomtask.network.Response;
import com.endrjudev.tomtomtask.network.ResponseStatus;
import com.endrjudev.tomtomtask.repository.NetworkRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class MockNetworkRepository implements NetworkRepository {

    @NonNull
    @Override
    public MutableLiveData<Response<DocsResponse>> getDocs(@NonNull String query, @NonNull MutableLiveData<Response<DocsResponse>> responseLiveData) {

        final List<Doc> mockList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            mockList.add(new Doc(Collections.singletonList("Author " + i), "Title " + i, Arrays.asList("1", "2", "3", "4", "5")));
        }

        responseLiveData.setValue(new Response<>(new DocsResponse(mockList), ResponseStatus.SUCCESS));

        return responseLiveData;
    }
}
