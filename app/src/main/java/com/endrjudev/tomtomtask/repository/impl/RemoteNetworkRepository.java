package com.endrjudev.tomtomtask.repository.impl;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.endrjudev.tomtomtask.model.DocsResponse;
import com.endrjudev.tomtomtask.network.CallType;
import com.endrjudev.tomtomtask.network.NetworkCall;
import com.endrjudev.tomtomtask.network.Response;
import com.endrjudev.tomtomtask.network.adapter.BaseAdapter;
import com.endrjudev.tomtomtask.network.callback.SimpleNetworkCallback;
import com.endrjudev.tomtomtask.repository.NetworkRepository;

public final class RemoteNetworkRepository implements NetworkRepository {

    private static final String BASE_URL = "openlibrary.org";

    @NonNull
    @Override
    public MutableLiveData<Response<DocsResponse>> getDocs(@NonNull String query, @NonNull MutableLiveData<Response<DocsResponse>> responseLiveData) {
        final NetworkCall<DocsResponse> call = new NetworkCall<>(CallType.GET, new BaseAdapter<>(DocsResponse.class));

        final Uri uri = new Uri.Builder()
                .scheme("https")
                .authority(BASE_URL)
                .appendPath("search.json")
                .appendQueryParameter("q", query)
                .build();

        call.executeCall(uri.toString(), new SimpleNetworkCallback<DocsResponse>() {
            @Override
            public void onSuccess(Response<DocsResponse> response) {
                responseLiveData.setValue(response);
            }

            @Override
            public void onFailure(Response<DocsResponse> response) {
                responseLiveData.setValue(response);
            }

            @Override
            public void onStart(Response<DocsResponse> response) {
                responseLiveData.setValue(response);
            }
        });

        return responseLiveData;
    }
}
