package com.endrjudev.tomtomtask.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.endrjudev.tomtomtask.model.DocsResponse;
import com.endrjudev.tomtomtask.network.Response;

public interface NetworkRepository {
    @NonNull
    MutableLiveData<Response<DocsResponse>> getDocs(@NonNull String query, @NonNull MutableLiveData<Response<DocsResponse>> responseLiveData);
}