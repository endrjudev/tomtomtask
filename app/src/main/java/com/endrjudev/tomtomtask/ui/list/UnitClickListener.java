package com.endrjudev.tomtomtask.ui.list;

import com.endrjudev.tomtomtask.model.Doc;

public interface UnitClickListener {
    void onUnitClick(Doc doc);
}
