package com.endrjudev.tomtomtask.ui.detail;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.endrjudev.tomtomtask.databinding.ItemIsbnBinding;

public class ISBNAdapter extends ListAdapter<String, ISBNAdapter.ISBNViewHolder> {

    ISBNAdapter() {
        super(new DiffUtil.ItemCallback<String>() {
            @Override
            public boolean areItemsTheSame(@NonNull String oldItem, @NonNull String newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areContentsTheSame(@NonNull String oldItem, @NonNull String newItem) {
                return oldItem.equals(newItem);
            }
        });
    }

    @NonNull
    @Override
    public ISBNViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ISBNViewHolder(ItemIsbnBinding.inflate(
                LayoutInflater.from(parent.getContext()),
                parent,
                false));
    }

    @Override
    public void onBindViewHolder(@NonNull ISBNViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    static class ISBNViewHolder extends RecyclerView.ViewHolder {

        private final ItemIsbnBinding binding;

        ISBNViewHolder(@NonNull ItemIsbnBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(String isbn) {
            binding.image.setBackgroundDrawable(null);
            binding.setNumber(isbn);
            binding.executePendingBindings();
        }
    }
}
