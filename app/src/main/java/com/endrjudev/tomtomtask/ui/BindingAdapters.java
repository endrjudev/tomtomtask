package com.endrjudev.tomtomtask.ui;

import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.endrjudev.tomtomtask.R;
import com.endrjudev.tomtomtask.network.CallType;
import com.endrjudev.tomtomtask.network.NetworkCall;
import com.endrjudev.tomtomtask.network.Response;
import com.endrjudev.tomtomtask.network.adapter.BitmapAdapter;
import com.endrjudev.tomtomtask.network.callback.SimpleNetworkCallback;

public class BindingAdapters {

    @BindingAdapter("loadImage")
    public static void loadImage(ImageView imageView, String isbn) {
        imageView.setImageDrawable(null);

        final NetworkCall<Bitmap> call = new NetworkCall<>(CallType.GET, new BitmapAdapter());

        final Uri uri = new Uri.Builder()
                .scheme("https")
                .authority("covers.openlibrary.org")
                .appendPath("b")
                .appendPath("isbn")
                .appendPath(isbn + "-S.jpg")
                .build();

        imageView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {

            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                call.cancelTask();
            }
        });

        call.executeCall(uri.toString(), new SimpleNetworkCallback<Bitmap>() {
            @Override
            public void onSuccess(Response<Bitmap> response) {
                imageView.setImageBitmap(response.getData());
            }

            @Override
            public void onFailure(Response<Bitmap> response) {
                imageView.setImageResource(R.drawable.ic_book);
            }
        });
    }
}