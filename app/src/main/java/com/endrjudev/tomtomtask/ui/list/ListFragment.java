package com.endrjudev.tomtomtask.ui.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.DividerItemDecoration;

import com.endrjudev.tomtomtask.R;
import com.endrjudev.tomtomtask.databinding.FragmentListBinding;
import com.endrjudev.tomtomtask.model.Doc;
import com.endrjudev.tomtomtask.model.DocsResponse;
import com.endrjudev.tomtomtask.network.Response;
import com.endrjudev.tomtomtask.viewmodel.ListViewModel;
import com.google.android.material.snackbar.Snackbar;

public class ListFragment extends Fragment implements UnitClickListener, SearchView.OnQueryTextListener {

    private FragmentListBinding binding;
    private ListViewModel viewModel;
    private UnitAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupToolbar();

        adapter = new UnitAdapter(this);
        binding.list.setAdapter(adapter);
        binding.search.setOnQueryTextListener(this);

        final DividerItemDecoration itemDecoration = new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.item_divider));
        binding.list.addItemDecoration(itemDecoration);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);
        viewModel.getDocs().observe(getViewLifecycleOwner(), this::handleResponse);

        if (getArguments() != null) {
            binding.search.setQuery(ListFragmentArgs.fromBundle(getArguments()).getQuery(), true);
        }
    }

    private void setupToolbar() {
        final AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity == null) {
            return;
        }
        activity.setSupportActionBar(binding.toolbar);
        NavigationUI.setupActionBarWithNavController(activity, Navigation.findNavController(requireView()));
    }

    private void handleResponse(Response<DocsResponse> response) {
        switch (response.getStatus()) {
            case SUCCESS:
                showProgress(false);
                adapter.submitList(response.getData().getDocList());
                break;
            case ERROR:
                showProgress(false);
                showError(R.string.error_fetch_data);
                break;
            case PROGRESS:
                showProgress(true);
                break;
        }
    }

    @Override
    public void onUnitClick(Doc doc) {
        Navigation.findNavController(requireView())
                .navigate(ListFragmentDirections.actionListFragmentToDetailFragment(doc));
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        viewModel.fetchUnits(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private void showProgress(boolean show) {
        binding.progress.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void showError(@StringRes int stringRes) {
        Snackbar.make(requireView(), stringRes, Snackbar.LENGTH_SHORT).show();
    }
}