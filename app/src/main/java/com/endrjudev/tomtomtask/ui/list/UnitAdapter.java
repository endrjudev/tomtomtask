package com.endrjudev.tomtomtask.ui.list;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.endrjudev.tomtomtask.databinding.ItemListBinding;
import com.endrjudev.tomtomtask.model.Doc;

public class UnitAdapter extends ListAdapter<Doc, UnitAdapter.UnitViewHolder> {

    @NonNull
    private UnitClickListener listener;

    UnitAdapter(@NonNull UnitClickListener listener) {
        super(Doc.callback);
        this.listener = listener;
    }

    @NonNull
    @Override
    public UnitViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final ItemListBinding binding = ItemListBinding.inflate(
                LayoutInflater.from(parent.getContext()),
                parent,
                false);
        return new UnitViewHolder(binding, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull UnitViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    static class UnitViewHolder extends RecyclerView.ViewHolder {

        @NonNull
        private final ItemListBinding binding;
        @NonNull
        private final UnitClickListener listener;

        UnitViewHolder(@NonNull ItemListBinding binding,
                       @NonNull UnitClickListener listener) {
            super(binding.getRoot());
            this.binding = binding;
            this.listener = listener;
        }

        void bind(final Doc model) {
            binding.setModel(model);
            binding.getRoot().setOnClickListener(v -> listener.onUnitClick(model));
            binding.executePendingBindings();
        }
    }
}