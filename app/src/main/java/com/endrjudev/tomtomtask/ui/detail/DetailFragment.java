package com.endrjudev.tomtomtask.ui.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.DividerItemDecoration;

import com.endrjudev.tomtomtask.R;
import com.endrjudev.tomtomtask.databinding.FragmentDetailBinding;
import com.endrjudev.tomtomtask.model.Doc;

public class DetailFragment extends Fragment implements View.OnClickListener {

    private FragmentDetailBinding binding;
    private ISBNAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentDetailBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupToolbar();

        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.title.setOnClickListener(this);
        binding.author.setOnClickListener(this);

        adapter = new ISBNAdapter();
        binding.isbnList.setAdapter(adapter);

        final DividerItemDecoration itemDecoration = new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.item_divider));
        binding.isbnList.addItemDecoration(itemDecoration);

        if (getArguments() != null) {
            final Doc doc = DetailFragmentArgs.fromBundle(getArguments()).getDoc();
            binding.setModel(doc);

            if (doc.getIsbnList() == null) {
                return;
            }
            if (doc.getIsbnList().size() > 5) {
                adapter.submitList(doc.getIsbnList().subList(0, 5));
                return;
            }
            adapter.submitList(doc.getIsbnList());
        }
    }

    private void setupToolbar() {
        final AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity == null) {
            return;
        }
        activity.setSupportActionBar(binding.toolbar);
        NavigationUI.setupActionBarWithNavController(activity, Navigation.findNavController(requireView()));
    }

    @Override
    public void onClick(View v) {
        final String newQuery;

        switch (v.getId()) {
            case R.id.author:
                newQuery = binding.author.getText().toString();
                break;
            case R.id.title:
                newQuery = binding.title.getText().toString();
                break;
            default:
                newQuery = "";
        }

        Navigation.findNavController(requireView())
                .navigate(DetailFragmentDirections.actionDetailFragmentToListFragment().setQuery(newQuery));
    }
}