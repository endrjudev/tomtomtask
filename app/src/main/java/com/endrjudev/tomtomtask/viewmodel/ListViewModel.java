package com.endrjudev.tomtomtask.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.endrjudev.tomtomtask.model.DocsResponse;
import com.endrjudev.tomtomtask.network.Response;
import com.endrjudev.tomtomtask.repository.NetworkRepository;
import com.endrjudev.tomtomtask.repository.impl.RemoteNetworkRepository;

public class ListViewModel extends ViewModel {

    private final NetworkRepository networkRepository = new RemoteNetworkRepository();

    private final MutableLiveData<Response<DocsResponse>> docs = new MutableLiveData<>();

    public void fetchUnits(@NonNull String query) {
        networkRepository.getDocs(query, docs);
    }

    public MutableLiveData<Response<DocsResponse>> getDocs() {
        return docs;
    }

}